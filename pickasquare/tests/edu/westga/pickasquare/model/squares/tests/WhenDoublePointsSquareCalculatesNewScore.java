package edu.westga.pickasquare.model.squares.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.squares.DoublePointsSquare;

/**
 * WhenDoublePointsSquareCalculatesNewScore defines test cases
 * for a DoublePointsSquare calculating a new score.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class WhenDoublePointsSquareCalculatesNewScore {

	private DoublePointsSquare pointsDoubler;
	
	/**
	 * No-op constructor.
	 */
	public WhenDoublePointsSquareCalculatesNewScore() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp()  {
		this.pointsDoubler = new DoublePointsSquare();
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.DoublePointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void newSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.pointsDoubler.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.DoublePointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void squaresScoreShouldHaveBeenCalculated() {
		this.pointsDoubler.calculateNewScore(100);
		assertTrue(this.pointsDoubler.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.DoublePointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void shouldGet0WhenDoubleScoreOf0() {
		int newScore = this.pointsDoubler.calculateNewScore(0);
		assertEquals(0, newScore);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.DoublePointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void shouldGet200WhenDoubleScoreOf100() {
		int newScore = this.pointsDoubler.calculateNewScore(100);
		assertEquals(200, newScore);
	}
	
}