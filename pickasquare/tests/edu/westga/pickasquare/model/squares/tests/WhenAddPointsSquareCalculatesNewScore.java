package edu.westga.pickasquare.model.squares.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.squares.AddPointsSquare;

/**
 * WhenAddPointsSquareCalculatesNewScore defines test cases
 * for an AddPointsSquare calculating a new score.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class WhenAddPointsSquareCalculatesNewScore {

	private AddPointsSquare onePointAdder;
	private AddPointsSquare tenPointAdder;
	
	/**
	 * No-op constructor.
	 */
	public WhenAddPointsSquareCalculatesNewScore() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp()  {
		this.onePointAdder = new AddPointsSquare(1);
		this.tenPointAdder = new AddPointsSquare(10);
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.AddPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void newOnePointSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.onePointAdder.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.AddPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void newTenPointSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.tenPointAdder.scoreHasBeenCalculated());
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.AddPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void onePointSquareShouldAdd1Point() {
		int newScore = this.onePointAdder.calculateNewScore(100);
		assertEquals(101, newScore);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.AddPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void onePointSquaresScoreShouldHaveBeenCalculated() {
		this.onePointAdder.calculateNewScore(100);
		assertTrue(this.onePointAdder.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.AddPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void tenPointSquareShouldAdd10Points() {
		int newScore = this.tenPointAdder.calculateNewScore(100);
		assertEquals(110, newScore);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.AddPointsSquare#calculateNewScore(int)}.
	 */
	@Test
	public void tenPointSquaresScoreShouldHaveBeenCalculated() {
		this.tenPointAdder.calculateNewScore(100);
		assertTrue(this.tenPointAdder.scoreHasBeenCalculated());
	}

}