package edu.westga.pickasquare.model.squares.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.squares.HalfPointsSquare;

/**
 * WhenHalfPointsSquareCalculatesNewScore defines test cases
 * for a HalfPointsSquare calculating a new score.
 * 
 * @author Nathan Beam
 * @version Spring, 2015
 */
public class WhenHalfPointsSquareCalculatesNewScore {

	private HalfPointsSquare pointsHalver;

	/**
	 * No-op constructor.
	 */
	public WhenHalfPointsSquareCalculatesNewScore() {
		// no-op
	}

	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp() {
		this.pointsHalver = new HalfPointsSquare();
	}

	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}
	 * .
	 */
	@Test
	public void newSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.pointsHalver.scoreHasBeenCalculated());
	}

	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}
	 * .
	 */
	@Test
	public void squaresScoreShouldHaveBeenCalculated() {
		this.pointsHalver.calculateNewScore(100);
		assertTrue(this.pointsHalver.scoreHasBeenCalculated());
	}

	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}
	 * .
	 */
	@Test
	public void shouldGet0WhenHalveScoreOf0() {
		int newScore = this.pointsHalver.calculateNewScore(0);
		assertEquals(0, newScore);
	}

	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.squares.HalfPointsSquare#calculateNewScore(int)}
	 * .
	 */
	@Test
	public void shouldGet50WhenHalveScoreOf100() {
		int newScore = this.pointsHalver.calculateNewScore(100);
		assertEquals(50, newScore);
	}

}