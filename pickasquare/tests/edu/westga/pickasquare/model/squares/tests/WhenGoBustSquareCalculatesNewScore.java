package edu.westga.pickasquare.model.squares.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.squares.GoBustSquare;

/**
 * WhenGoBustSquareCalculatesNewScore defines test cases
 * for a GoBustSquare calculating a new score.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class WhenGoBustSquareCalculatesNewScore {
	
	private GoBustSquare testSquare;
	
	/**
	 * No-op constructor
	 */
	public WhenGoBustSquareCalculatesNewScore() {
		// no-op
	}

	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp()  {
		this.testSquare = new GoBustSquare();
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.GoBustSquare#calculateNewScore(int)}.
	 */
	@Test
	public void newSquaresScoreShouldNotHaveBeenCalculated() {
		assertFalse(this.testSquare.scoreHasBeenCalculated());
	}

	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.GoBustSquare#calculateNewScore(int)}.
	 */
	@Test
	public void squaresScoreShouldHaveBeenCalculated() {
		this.testSquare.calculateNewScore(100);
		assertTrue(this.testSquare.scoreHasBeenCalculated());
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.GoBustSquare#calculateNewScore(int)}.
	 */
	@Test
	public void shouldGet0WhenOldScoreIs0() {
		int newScore = this.testSquare.calculateNewScore(0);
		assertEquals(0, newScore);
	}
	
	/**
	 * Test method for {@link edu.westga.pickasquare.model.squares.GoBustSquare#calculateNewScore(int)}.
	 */
	@Test
	public void shouldGet0WhenOldScoreIs100() {
		int newScore = this.testSquare.calculateNewScore(100);
		assertEquals(0, newScore);
	}
	
}
