package edu.westga.pickasquare.model.players.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.players.RegularPlayer;

/**
 * WhenRegularPlayerSetsScore defines test cases
 * for a RegularPlayer setting a new score.
 * 
 * @author Nathan Beam
 * @version Spring, 2015
 */
public class WhenRegularPlayerSetsScore {


	
	private RegularPlayer testPlayer;
	
	/**
	 * No-op constructor.
	 */
	public WhenRegularPlayerSetsScore() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp() {
		this.testPlayer = new RegularPlayer();
	}
	

	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.players.RegularPlayer#setScore(int)}
	 * .
	 */
	@Test
	public void shouldSetScoreTo100() {
		this.testPlayer.setScore(100);
		assertEquals(100, this.testPlayer.getScore());
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.players.RegularPlayer#setScore(int)}
	 * .
	 */
	@Test
	public void shouldSetScoreTo0() {
		this.testPlayer.setScore(0);
		assertEquals(0, this.testPlayer.getScore());
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.players.RegularPlayer#setScore(int)}
	 * .
	 */
	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentException() {
		this.testPlayer.setScore(-1);
	}

}
