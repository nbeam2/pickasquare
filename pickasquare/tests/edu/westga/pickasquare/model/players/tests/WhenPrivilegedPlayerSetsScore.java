package edu.westga.pickasquare.model.players.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.westga.pickasquare.model.players.PrivilegedPlayer;

/**
 * WhenPrivilegedPlayerSetsScore defines test cases
 * for a PrivilegedPlayer setting a new score.
 * 
 * @author Nathan Beam
 * @version Spring, 2015
 */
public class WhenPrivilegedPlayerSetsScore {


	
	private PrivilegedPlayer testPlayer;
	
	/**
	 * No-op constructor.
	 */
	public WhenPrivilegedPlayerSetsScore() {
		// no-op
	}
	
	/**
	 * Creates the test objects.
	 */
	@Before
	public void setUp() {
		this.testPlayer = new PrivilegedPlayer();
	}
	

	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.players.PrivilegedPlayer#setScore(int)}
	 * .
	 */
	@Test
	public void shouldSetScoreTo200() {
		this.testPlayer.setScore(100);
		assertEquals(200, this.testPlayer.getScore());
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.players.PrivilegedPlayerr#setScore(int)}
	 * .
	 */
	@Test
	public void shouldSetScoreTo0() {
		this.testPlayer.setScore(0);
		assertEquals(0, this.testPlayer.getScore());
	}
	
	/**
	 * Test method for
	 * {@link edu.westga.pickasquare.model.players.PrivilegedPlayer#setScore(int)}
	 * .
	 */
	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentException() {
		this.testPlayer.setScore(-1);
	}

}
