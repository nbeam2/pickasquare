package edu.westga.pickasquare.controllers;

import edu.westga.pickasquare.model.GameBoard;
import edu.westga.pickasquare.model.players.Player;
import edu.westga.pickasquare.model.squares.GameSquare;

/**
 * GameController instances manage the play of one
 * game of play-or-hold.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class GameController {
	
	private Player currentPlayer;
	private Player otherPlayer;
	private GameBoard board;
	private boolean hold;

	/**
	 * Creates a new GameController object to manage the
	 * play of the specified Players. 
	 * 
	 * @precondition 	theBoard != null && player1 != null && player2 != null
	 * @postcondition	getCurrentPlayer().equals(player1)
	 * 
	 * @param aBoard	the board on which the game is played
	 * @param player1	one of the Players
	 * @param player2	the other RegularPlayer
	 */
	public GameController(GameBoard aBoard, Player player1, Player player2) {
		if (aBoard == null) {
			throw new IllegalArgumentException("Game board is null");
		}
		if (player1 == null) {
			throw new IllegalArgumentException("RegularPlayer 1 is null");
		}
		if (player2 == null) {
			throw new IllegalArgumentException("RegularPlayer 2 is null");
		}
		
		this.board = aBoard;
		this.currentPlayer = player1;
		this.otherPlayer = player2;
	}

	/**
	 * Returns the game's current player.
	 * 
	 * @precondition 	none
	 * @return 			the currentPlayer
	 */
	public Player getCurrentPlayer() {
		return this.currentPlayer;
	}

	/**
	 * Directs the GameController to make a move for the player
	 * using the selected GameSquare.
	 * 
	 * @precondition 	0 <= gameSquareId <= 15
	 * 					
	 * @postcondition 	none
	 * 
	 * @param gameSquareId	the ID of the AbstractGameSquare selected
	 * 						by the current player.
	 */
	public void play(int gameSquareId) {
		GameSquare square = this.board.getSquare(gameSquareId);
		this.currentPlayer.setScore(
						square.calculateNewScore(
								this.currentPlayer.getScore()));
		if (!this.hold) {
			this.swapPlayers();	
		}
	}
	
	
	/**
	 * Initiates the Hold system for a player. 
	 * When activated, players will no longer 
	 * switch out, and the non-holding player 
	 * will play until all squares are clicked 
	 * or they decide to hold as well
	 * 
	 * @precondition  none
	 * @postcondition hold is active for the current player
	 * 
	 * @return 		the current value of the Hold variable. 
	 * 		   		If true, the game is over.
	 */
	public boolean hold() {
		boolean currentValue = this.hold;
		this.hold = true;
		return currentValue;
	}

	private void swapPlayers() {
		Player playerHolder = this.currentPlayer;
		this.currentPlayer = this.otherPlayer;
		this.otherPlayer = playerHolder;
	}
}
