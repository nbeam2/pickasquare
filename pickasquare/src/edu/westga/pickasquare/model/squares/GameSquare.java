package edu.westga.pickasquare.model.squares;

/**
 * GameSquare defines the interface for a square
 * on the board on which the players make their moves.
 * <p>
 * A newly created GameSquare should indicate that
 * its score has not been calculated. Then, when
 * the square is selected by a player, it calculates
 * a new score for that player and modifies itself to
 * indicate that it has already been selected.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public interface GameSquare {

	/**
	 * Calculates the new score to be given the player who selected
	 * this GameSquare, marking this GameSquare as having had its
	 * score calculated.
	 * 
	 * @precondition	!scoreHasBeenCalculated() && 
	 * 					oldScore >= 0
	 * @postcondition	scoreHasBeenCalculated()
	 * 
	 * @return	a new score >= 0, to give to the player who 
	 * 			selected this square
	 * 
	 * @param oldScore	the player's score before s/he 
	 * 					selected this square
	 */
	int calculateNewScore(int oldScore);

	/**
	 * Returns a description of this square that can be
	 * displayed in a GUI to indicate what kind of
	 * square this is.
	 * 
	 * @precondition	none
	 * @return			the square's description
	 */
	String getDescription();

	/**
	 * Returns true iff this square was selected earlier
	 * in the game.
	 * 
	 * @precondition 	none
	 * @return	whether the square has already calculated the score
	 */
	boolean scoreHasBeenCalculated();
}