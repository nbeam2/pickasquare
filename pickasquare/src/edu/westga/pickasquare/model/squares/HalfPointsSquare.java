package edu.westga.pickasquare.model.squares;

/**
 * HalfPointsSquare defines a GameSquare that halves
 * the player's score when selected.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class HalfPointsSquare extends AbstractGameSquare {
		
	/**
	 * Creates a new HalfPointsSquare instance.
	 * 
	 * @precondition	none
	 * @postcondition	!scoreHasBeenCalculated()
	 */
	public HalfPointsSquare() {
		super();
	}

	/**  
	 * Implements the method by halving the specified score.
	 * 
	 * @precondition	oldScore >= 0
	 * @postcondition	scoreHasBeenCalculated()
	 * @return oldScore / 2
	 */
	@Override
	public int calculateNewScore(int oldScore) {
		super.calculateNewScore(oldScore);
		return  oldScore / 2;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.GameSquare#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Half D:";
	}
}

