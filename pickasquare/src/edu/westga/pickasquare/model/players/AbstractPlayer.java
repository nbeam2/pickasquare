package edu.westga.pickasquare.model.players;

/**
 * AbstractPlayer defines an abstract outline of functions
 * belonging to the Player type. The primary function of a 
 * Player is to store it's score, and, in the case of a Privileged 
 * Player, add the bonus value.
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public abstract class AbstractPlayer implements Player  {
	
	private int score = 0;
	/**
	 * Creates a new RegularPlayer instance.
	 * 
	 * @precondition 	none
	 * @postcondition	getScore() == 0
	 */
	public AbstractPlayer() {
		this.score = 0;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.players.Player#setScore(int)
	 */
	@Override
	public void setScore(int score) {
		if (score < 0) {
			throw new IllegalArgumentException("Score to set is less than 0!");
		}
		this.score = score;
	}

	/* (non-Javadoc)
	 * @see edu.westga.pickasquare.model.players.Player#getScore()
	 */
	@Override
	public int getScore() {
		return this.score;
	}
}
