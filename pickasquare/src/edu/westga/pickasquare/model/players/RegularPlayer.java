package edu.westga.pickasquare.model.players;


/**
 * RegularPlayer represents a person playing the pick-a-square game
 * following the normal rules of the game.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public final class RegularPlayer extends AbstractPlayer {
	


	/**
	 * Creates a new RegularPlayer instance.
	 * 
	 * @precondition 	none
	 * @postcondition	getScore() == 0
	 */
	public RegularPlayer() {
		super();
	}

	/**
	 * Sets the player's score.
	 * 
	 * @precondition 	score >= 0
	 * @postcondition	getScore() == score
	 * 
	 * @param score the score to set
	 */
	public void setScore(int score) {
		super.setScore(score);
	}

	/**
	 * Returns the player's score.
	 * 
	 * @precondition	none
	 * @return the score
	 */
	public int getScore() {
		return super.getScore();
	}
	
}