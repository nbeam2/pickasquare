package edu.westga.pickasquare.model.players;

/**
 * Player defines the interface for a new player 
 * in the game.
 * <p>
 * A player will store score values, and a privileged player
 * will receive 100 bonus points per move.  
 * 
 * @author Nathan Beam
 * @version Spring 2015
 */
public interface Player {

	/**
	 * Sets the player's score.
	 * 
	 * @precondition 	score >= 0
	 * @postcondition	getScore() == score
	 * 
	 * @param score the score to set
	 */
	void setScore(int score);

	/**
	 * Returns the player's score.
	 * 
	 * @precondition	none
	 * @return the score
	 */
	int getScore();

}