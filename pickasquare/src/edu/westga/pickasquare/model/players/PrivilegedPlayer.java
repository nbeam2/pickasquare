package edu.westga.pickasquare.model.players;

/**
 * RegularPlayer represents a person playing the pick-a-square game
 * following the normal rules of the game.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public final class PrivilegedPlayer extends AbstractPlayer {

   /**
	 * Creates a new PivelegedPlayer instance.
	 * 
	 * @precondition 	none
	 * @postcondition	getScore() == 0
	 */
	public PrivilegedPlayer() {
		super();
	}

	/**
	 * Sets the player's score.
	 * 
	 * @precondition 	score >= 0
	 * @postcondition	getScore() == score
	 * 
	 * @param score 	the base score to set, 100 is added if score != 0
	 */
	public void setScore(int score) {
		if (score <= 0) {
			super.setScore(score);		
		} else {
			super.setScore(score + 100); 
		}
	}

	/**
	 * Returns the player's score.
	 * 
	 * @precondition	none
	 * @return the score
	 */
	public int getScore() {
		return super.getScore();
	}
}