package edu.westga.pickasquare.view;

import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

/**
 * GuiCodeBehind defines the JavaFX "controller" for Gui.fxml.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public class GuiCodeBehind {
	
	private ViewModel theViewModel;
	
	private List<Button> buttons;
	private List<Node> boardNodes;
	
	@FXML
    private GridPane boardGridPane;

    @FXML
    private Button holdButton;	
    
    @FXML
    private Label gameStatuslabel;
    
    @FXML
    private TextField player1ScoreTextField;
    
    @FXML
    private TextField player2ScoreTextField;
    
    @FXML
    private ImageView scaryImage1;

    @FXML
    private ImageView scaryImage2;
    
    @FXML
    private ImageView victoryImage;
    
    @FXML
    private ImageView coolImage;
	
	/**
	 * Creates a new GuiCodeBehind instance.
	 * 
	 * @precondition	none
	 * @postcondition	the object and its view model exist
	 */
	public GuiCodeBehind() {
		this.theViewModel = new ViewModel();
		this.createButtons();
	}
    
    /**
	 * Initializes the GUI components, binding them to the view model properties
	 * and setting their event handlers.
	 */
	@FXML
	public void initialize() {
		this.bindGuiComponentsToViewModel();
		this.addButtonsToBoard();
		this.boardNodes = this.boardGridPane.getChildren();
		this.setEventActions();
	}
	
	private void createButtons() {
		this.buttons = new ArrayList<Button>();
		for (int i = 0; i < 16; i++) {
			this.buttons.add(this.createButtonWithId(i));
		}
	}
	
	private Button createButtonWithId(int id) {
		String buttonId = "" + (id + 1);
		Button aButton = new Button("   ?   ");
		aButton.setMaxWidth(Double.MAX_VALUE);
		aButton.setId(buttonId);
		return aButton;
	}

	private void addButtonsToBoard() {		
		for (int row = 0; row < 4; row++) {
			this.addARow(row);
		}	
	}

	private void addARow(int row) {
		for (int column = 0; column < 4; column++) {
			this.boardGridPane.add(this.buttons.remove(0), column, row);
		}
	}

	private void bindGuiComponentsToViewModel() {
		this.player1ScoreTextField.textProperty().
				bindBidirectional(this.theViewModel.player1ScoreProperty());
		this.player2ScoreTextField.textProperty().
				bindBidirectional(this.theViewModel.player2ScoreProperty());
		this.gameStatuslabel.textProperty().
				bindBidirectional(this.theViewModel.gameStatusProperty());
		this.victoryImage.imageProperty().
				bindBidirectional(this.theViewModel.victoryImageProperty());
		this.scaryImage1.imageProperty().
			bindBidirectional(this.theViewModel.scaryImageProperty());
		this.scaryImage2.imageProperty().
			bindBidirectional(this.theViewModel.scaryImageProperty());
		this.coolImage.imageProperty().
			bindBidirectional(this.theViewModel.coolImageProperty());
	}

	private void setEventActions() {
		for (Node aButton: this.boardNodes) {
			aButton.setOnMousePressed(event -> this.handleBoardButtonClick(aButton));
			aButton.setOnMouseReleased(event -> this.theViewModel.isEnd());
		}
		this.holdButton.setOnMouseClicked(event -> this.theViewModel.handleHold());
			
	}

	private void handleBoardButtonClick(Node aButton) {
		aButton.setDisable(true);
		this.holdButton.requestFocus();
		this.theViewModel.play(aButton.getId());
		((Button) aButton).setText(this.theViewModel.selectedSquareDescription());
	}

}