package edu.westga.pickasquare.view;

import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JOptionPane;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import edu.westga.pickasquare.controllers.GameController;
import edu.westga.pickasquare.model.GameBoard;
import edu.westga.pickasquare.model.GameBoardFactory;
import edu.westga.pickasquare.model.HalfDoublePointsBoardFactory;
import edu.westga.pickasquare.model.players.Player;
import edu.westga.pickasquare.model.players.PrivilegedPlayer;
import edu.westga.pickasquare.model.players.RegularPlayer;

/**
 * ViewModel defines the view model for the play-or-hold application.
 * 
 * @author CS 1302
 * @version Spring, 2015
 */
public final class ViewModel {
	

	private String descriptionOfSelectedSquare;
	
	private final Player player1;
	private final Player player2;
	private final GameBoard theBoard;
	
	private final GameController theGameController;
	
	private StringProperty player1Score;
	private StringProperty player2Score;
	private StringProperty gameStatus;
	
	private int index;

	private boolean hold;
	
	private ObjectProperty<Image> victoryImage;
	private ObjectProperty<Image> scaryImage;
	private ObjectProperty<Image> coolImage;

	
	/**
	 * Creates a new ViewModel instance.
	 * 
	 * @precondition none
	 * @postcondition the object and its properties exist
	 */
	public ViewModel() {
		this.descriptionOfSelectedSquare = "";
		
		this.player1 = new RegularPlayer();
		this.player2 = new PrivilegedPlayer();
		
		GameBoardFactory factory = new HalfDoublePointsBoardFactory(true);
		this.theBoard = factory.getGameBoard();
		
		this.theGameController = new GameController(this.theBoard, this.player1, this.player2);
		
		this.player1Score = new SimpleStringProperty("0");
		this.player2Score = new SimpleStringProperty("0");
		this.gameStatus = new SimpleStringProperty("Player 1's turn");
		this.index = 1;
		Image initialImage = new Image(getClass().getResourceAsStream("/resources/default.png"));
		this.victoryImage = new SimpleObjectProperty<Image>(initialImage);
		this.scaryImage = new SimpleObjectProperty<Image>(initialImage);
		this.coolImage = new SimpleObjectProperty<Image>(initialImage);
	}
	
	/**
	 * Returns the property that represents the score for player 1.
	 * 
	 * @precondition none
	 * @return		 the property 
	 */
	public StringProperty player1ScoreProperty() {
		return this.player1Score;
	}
	
	/**
	 * Returns the property that represents the score for player 2.
	 * 
	 * @precondition none
	 * @return		 the property 
	 */
	public StringProperty player2ScoreProperty() {
		return this.player2Score;
	}
	
	/**
	 * Returns the property that represents the current player's turn
	 * 
	 * @precondition none
	 * @return		 the property 
	 */
	public StringProperty gameStatusProperty() {
		return this.gameStatus;
	}
	
	/**
	 * Returns the property that represents the victory image if the secret condition is met
	 * 
	 * @precondition none
	 * @return		 the property 
	 */
	public Property<Image> victoryImageProperty() {
		return this.victoryImage;
	}
	
	/**
	 * Returns the property that represents the spooky image if the secret condition is met
	 * 
	 * @precondition none
	 * @return		 the property 
	 */
	public Property<Image> scaryImageProperty() {
		return this.scaryImage;
	}
	
	/**
	 * Returns the property that represents the cool image if the secret condition is met
	 * 
	 * @precondition none
	 * @return		 the property 
	 */
	public Property<Image> coolImageProperty() {
		return this.coolImage;
	}

	/**
	 * Tells the game controller to carry out the current
	 * player's move.
	 * 
	 * @precondition  	1 <= gameSquareId <= 16
	 * @postcondition	the current player's score reflects this move, &&
	 * 					currentMoveValue() returns the value of the
	 * 
	 * @param gameSquareId	the ID of the selected control in the GUI
	 */
	public void play(String gameSquareId) {
		int squareIndex = Integer.parseInt(gameSquareId) - 1;
		this.theGameController.play(squareIndex);
		this.descriptionOfSelectedSquare = 
						this.theBoard.getSquare(squareIndex).getDescription();
		this.player1Score.setValue("" + this.player1.getScore());
		this.player2Score.setValue("" + this.player2.getScore());
		if (!this.hold) {
			this.updateStatus();
		}
		this.index++;
	}

	/**
	 * Returns the value of the current "move".
	 * 
	 * @precondition	none
	 * @return			the value of the selected game board item 
	 */
	public String selectedSquareDescription() {
		return this.descriptionOfSelectedSquare;
	}

	
	/**
	 * Handles the click of the hold button
	 * If the gameController's hold value is true 
	 * (This is the second hold) then the game is over.
	 * 
	 * @precondition none
	 * @postcondition current player will no longer play
	 * 				  if both players are holding, gameOver()
	 * 
	 */
	public void handleHold() {
		this.hold = true;
		if (this.theGameController.hold()) {
			this.gameOver();
		}
	}
	
	/**
	 * Checks to see if all squares have been clicked
	 * by comparing to a turn-counter index. Probably
	 * the worst possible way to do this. Oh well.
	 * 
	 * @precondition none
	 * @postcondition if 16 turns have been played, gameOver()
	 */
	public void isEnd() {
		if (this.index == 17) {
			this.gameOver();
		}
	}
	
	
	/**
	 * End of game messages
	 * 
	 * @precondition both players have held or index == 17
	 * @postcondition the game is over, system will exit.
	 */
	private void gameOver() {
		if (this.player1.getScore() >= 2000 || this.player2.getScore() >= 2000) {
			this.secretEnd();			
		} else if (this.player1.getScore() > this.player2.getScore()) {
			JOptionPane.showMessageDialog(null, "Player 1, you win!", "Congrats!", 1);
			System.exit(0);
		} else if (this.player2.getScore() > this.player1.getScore()) {
			JOptionPane.showMessageDialog(null, "Player 2, you win, you cheating loser.", "Meh...", 1);
			System.exit(0);
		} else {
			JOptionPane.showMessageDialog(null, "What the-! It's a tie!", "WHOA!" , 1);
			System.exit(0);
		}
	}

	private void secretEnd() {
		this.victoryImage.setValue(new Image(getClass().getResourceAsStream("/resources/image1.gif")));
		this.scaryImage.setValue(new Image(getClass().getResourceAsStream("/resources/image 2.gif")));
		this.coolImage.setValue(new Image(getClass().getResourceAsStream("/resources/cool.gif")));
		try {
			Media fanfare = new Media(new URL("http://www.funnyproelites.com/fanfare.mp3").toString());
			MediaPlayer mediaPlayer = new MediaPlayer(fanfare);
			mediaPlayer.play();
		} catch (MalformedURLException exception) {
			exception.printStackTrace();
		}		
	}

	private void updateStatus() {
		this.gameStatus.setValue("Player " + ((this.index % 2) + 1)  + "'s turn");
	}
}
